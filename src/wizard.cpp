#include <QAbstractButton>

#include "wellcome_page.h"
#include "portselect_page.h"
#include "dumpsavesetup.h"
#include "readpage.h"
#include "encodingsavesetup.h"
#include "encodingpage.h"
#include "finish_page.h"

#include "wizard.h"

static const struct btn {
    QWizard::WizardButton btn;
    QString rusName;
} buttons [] = {
    { QWizard::BackButton, QObject::trUtf8("< Назад") },
    { QWizard::NextButton, QObject::trUtf8("Далее >") },
    { QWizard::CommitButton, QObject::trUtf8("Принять") },
    { QWizard::FinishButton, QObject::trUtf8("Готово") },
    { QWizard::CancelButton, QObject::trUtf8("Отмена") }
};

wizard::wizard(QWidget *parent)
    : QWizard(parent)
{
    // создать страницы
    addPage(new Wellcome_page);
    addPage(new PortSelect_page);
    addPage(new DumpSaveSetup);
    addPage(new ReadPage);
    addPage(new encodingSaveSetup);
    addPage(new encodingpage);
    addPage(new Finish_page);

    for (uint i = 0; i < sizeof(buttons) / sizeof(btn); ++i) {
        setButtonText(buttons[i].btn, buttons[i].rusName);
    }

    setWindowTitle(trUtf8("Мастер считывания АРВ"));
}

wizard::~wizard()
{
}

void wizard::accept()
{

    QDialog::accept();
}
