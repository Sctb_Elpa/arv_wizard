#ifndef PORTSELECT_PAGE_H
#define PORTSELECT_PAGE_H

#include <QWizardPage>
#include <stdint.h>

class QLabel;
class QTextEdit;
class QProcess;
class QComboBox;
class QPushButton;

class PortpropertyHolder : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(int addr READ getAddr)
    Q_PROPERTY(int serial READ getSerial)
public:
    PortpropertyHolder(uint8_t addr = 0, uint16_t serial = 0,
                       QWidget* parent = NULL);

    int getAddr() const { return addr; }
    int getSerial() const { return serial; }

    void setAddr(uint8_t addr) { this->addr = addr; }
    void setSerial(uint16_t serial) { this->serial = serial; }

private:
    uint8_t addr;
    uint16_t serial;
};

class PortSelect_page : public QWizardPage
{
    Q_OBJECT
public:
    explicit PortSelect_page(QWidget *parent = 0);

    void initializePage(); // обновить страницу, когда нажимали next
    bool isComplete() const; // вернуть, готова-ли страница
    // нужно посылать сигнал completeChanged(), тогда

private:
    QLabel *statusLabel;
    QTextEdit *console_window;
    QProcess *Shell;
    QComboBox *ports;
    QPushButton *testbutton;

    void found(uint8_t addr, uint16_t serial);
    PortpropertyHolder* portpropertyHolder;

    void resetFound();
    void resetTesstButton();

private slots:
    void testPort();
    void showOutput();
    void resetWizardCancel();
    void cancel_search();
};

#endif // PORTSELECT_PAGE_H
