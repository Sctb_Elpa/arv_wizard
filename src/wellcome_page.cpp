#include <QLabel>
#include <QVBoxLayout>

#include "wellcome_page.h"

Wellcome_page::Wellcome_page(QWidget *parent) :
    QWizardPage(parent)
{
    setTitle(trUtf8("Приветствуем!"));
    //setPixmap(QWizard::WatermarkPixmap, QPixmap(":/images/watermark1.png"));

    label = new QLabel(trUtf8("Вы запустили программу считывания "
                              "автаномного регистратора волнения (АРВ).\n\n"
                              "Этот мастер поможет Вам пройти все этапы "
                              "до получения готовых данных."));
    label->setWordWrap(true);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(label);
    setLayout(layout);
}
