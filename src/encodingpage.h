#ifndef ENCODINGPAGE_H
#define ENCODINGPAGE_H

#include <QWizardPage>

class QLabel;
class QTextEdit;
class QProcess;
class QProgressBar;
class QLabel;
class QRegExp;
class QTextCodec;

class encodingpage : public QWizardPage
{
    Q_OBJECT
public:
    explicit encodingpage(QWidget *parent = 0);

    void initializePage(); // обновить страницу, когда нажимали next
    bool isComplete() const; // вернуть, готова-ли страница
    // нужно посылать сигнал completeChanged(), тогда


private slots:
    void startEncoding();
    void resetWizardButtons();
    void ParceSTDOutput();
    void ParceSTDErr();
    void decodeProcessFinised();
    void decodeError(const QString& errstr);

private:
    QLabel *status;
    QTextEdit *console_window;
    QProcess *Shell;
    QProgressBar* progress;
    QString lastError;
    float done_pr;
    int done_segments;
    QTextCodec *codec;

    static QRegExp Endl;
};

#endif // ENCODINGPAGE_H
