#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QDir>
#include <QFileDialog>
#include <QDebug>
#include <QFile>

#include "dumpsavesetup.h"

DumpSaveSetup::DumpSaveSetup(QWidget *parent) :
    QWizardPage(parent)
{
    setTitle(trUtf8("Сохранение дампа-файла"));
    setSubTitle(trUtf8("Необходимо произвезти полно считывание памяти АРВ и "
                       "Выберите место, куда он будет сохранен дамп-файл"));

    QHBoxLayout *mainlayout = new QHBoxLayout;

    dumpfilename = new QLineEdit;
    mainlayout->addWidget(dumpfilename);
    registerField("dumpFileName", dumpfilename, "text");
    connect(dumpfilename, SIGNAL(textChanged(QString)), this,
            SIGNAL(completeChanged()));

    QPushButton *browse = new QPushButton(trUtf8("..."));
    connect(browse, SIGNAL(clicked()), this, SLOT(browseSaveAs()));

    mainlayout->addWidget(browse);

    setLayout(mainlayout);
}

void DumpSaveSetup::initializePage()
{
    if (dumpfilename->text().isEmpty()) {
        dumpfilename->setText(QDir::homePath() + trUtf8("/ARV_%1.dump").arg(
                                  field("devSerial").toString()));
    }
}

bool DumpSaveSetup::isComplete() const
{
    return !dumpfilename->text().isEmpty() &&
            !dumpfilename->text().endsWith("/") &&
            QDir(dumpfilename->text()).cdUp();
}

void DumpSaveSetup::browseSaveAs()
{
    QDir base(dumpfilename->text());
    if (base.path().isEmpty())
        base = QDir::home();

    QString dialogres =
            QFileDialog::getSaveFileName(this,
                                         trUtf8("Сохранить дамп-файл как.."),
                                         base.path(), ".dump");
    if (!dialogres.isEmpty()) {
        if (!dialogres.endsWith(".dump")) {
            dialogres.append(".dump");
        }
        dumpfilename->setText(dialogres);
    }
}
