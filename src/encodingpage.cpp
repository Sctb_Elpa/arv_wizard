#include <QVBoxLayout>
#include <QLabel>
#include <QProcess>
#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include <QTextEdit>
#include <QTimer>
#include <QAbstractButton>
#include <QMessageBox>
#include <QTextCodec>
#include <QTimerEvent>

#include "encodingpage.h"

#ifdef Q_OS_UNIX
#define PYTHON3 QObject::tr("python3")
#elif defined(Q_OS_WIN32)
#define PYTHON3 (QDir::currentPath() + "/py/python")
#endif

QRegExp encodingpage::Endl("[\\r\\n]$");

encodingpage::encodingpage(QWidget *parent) :
    QWizardPage(parent)
{
    setTitle(trUtf8("Декодирование данных"));
    setSubTitle(trUtf8("Идет декодирование дамп-файла в формат CSV"));

    QVBoxLayout *rootLayout = new QVBoxLayout;

    status = new QLabel;
    rootLayout->addWidget(status);

    console_window = new QTextEdit;
    console_window->setReadOnly(true);
    rootLayout->addWidget(console_window);

    Shell = NULL;

    codec  = QTextCodec::codecForLocale();

    setLayout(rootLayout);
}

void encodingpage::initializePage()
{
    QTimer::singleShot(10, this, SLOT(startEncoding()));
}

bool encodingpage::isComplete() const
{
    return Shell && lastError.isEmpty();
}

void encodingpage::startEncoding()
{
    status->setText(trUtf8("Идет декодирование"));

    if (Shell) {
        Shell->terminate();
        Shell->deleteLater();
    }

    console_window->clear();

    Shell = new QProcess(this);
    Shell->setReadChannelMode(QProcess::MergedChannels);

    connect (Shell, SIGNAL(readyReadStandardOutput()),
             this, SLOT(ParceSTDOutput()));
    connect (Shell, SIGNAL(readyReadStandardError()),
             this, SLOT(ParceSTDErr()));

    QStringList params;
    params << "-u" // see: http://stackoverflow.com/questions/107705/disable-output-buffering
           << QDir::currentPath() + "/py/ARPDumpAnalyser.py"
           << "-o"
           << field("resultDir").toString();

    if (field("saveHeads").toBool()) {
        params << "--saveheads";
    }

    params
        #ifdef QT_DEBUG
        #ifdef Q_OS_WIN32
            << "h:/Razrab/py/ARPDumpAnalyser/S-11_2013-05-24.ARP.dump"
           #else
            << "/media/truecrypt1/Razrab/py/ARPDumpAnalyser/S-11_2013-05-24.ARP.dump"
           #endif /* Q_OS_WIN32  */
           #else
            << field("dumpFileName").toString()
           #endif /* QT_DEBUG */
               ;

    Shell->start(PYTHON3, params, QIODevice::ReadOnly);
    if (!Shell->waitForStarted(10000)) {
        QMessageBox::critical(this, trUtf8("Ошибка запуска"),
                              trUtf8("Не удалось запустить дочерний процесс python"
                             #ifdef Q_OS_WIN32
                                     ", убедитесь в пристутствии этого файла в каталоге py"
                             #endif
                                     ));
        resetWizardButtons();
    } else {
        connect(wizard()->button(QWizard::CancelButton), SIGNAL(clicked()), Shell,
                SLOT(terminate()));
        connect(wizard()->button(QWizard::BackButton), SIGNAL(clicked()), Shell,
                SLOT(terminate()));
        connect(Shell, SIGNAL(finished(int)), this, SLOT(decodeProcessFinised()));
    }
}

void encodingpage::resetWizardButtons()
{
    disconnect(wizard()->button(QWizard::CancelButton),
               SIGNAL(clicked()), Shell, SLOT(terminate()));
    disconnect(wizard()->button(QWizard::BackButton), SIGNAL(clicked()), Shell,
               SLOT(terminate()));
}

void encodingpage::ParceSTDOutput()
{
    QString bytes_stdout = codec->toUnicode(Shell->readAllStandardOutput());
    QStringList lines = QString(bytes_stdout).split("\n");

    foreach (QString line, lines) {
        line.remove(Endl);
        if (line.isEmpty()) {
            continue;
        }
        console_window->append(line);
    }
}

void encodingpage::ParceSTDErr()
{
    QString bytes_stdout = codec->toUnicode(Shell->readAllStandardError());
    QStringList lines = QString(bytes_stdout).split("\n");

    QRegExp fatalError("\\[WARNING\\].*");

    foreach (QString line, lines) {
        line.remove(Endl);
        if (line.isEmpty()) {
            continue;
        }
        if (fatalError.indexIn(line) < 0) {
            decodeError(line);
        }

        console_window->append(line);
    }
}

void encodingpage::decodeProcessFinised()
{
    resetWizardButtons();

    status->setText(trUtf8("Декодирование завершено"));

    if (!lastError.isEmpty()) {
        int res = QMessageBox::critical(this, trUtf8("Ошибка декодирования"), trUtf8(
                                            "Во время декодирования произошла ошибка \"%1\" "
                                            "которая привела к невозможности продолжить."
                                            "\nРекомендуется сохранить журнал для изучения"
                                            "причины ошибки.").arg(lastError),
                                        trUtf8("Закрыть"), trUtf8("Повтор"), trUtf8("Сохранить и закрыть"));
        switch (res) {
        case 1:
            lastError.clear();
            initializePage();
            break;
        case 2:
            QString logfile =
                    QFileDialog::getSaveFileName(this,
                                                 trUtf8("Сохранить отчет"), "",
                                                 "*.log");
            if (!logfile.isEmpty()) {
                if (!logfile.endsWith(".log")) {
                    logfile.append(".log");
                }
                QFile file(logfile);

                if (file.open(QIODevice::ReadWrite)) {
                    QTextStream stream(&file);
                    stream << console_window->toPlainText();
                    file.flush();
                    file.close();
                } else {
                    QMessageBox::critical(this, trUtf8("Ошибка записи"),
                                          trUtf8("Не удалось записать отчет в файл %1")
                                          .arg(logfile));
                    return;
                }
            }
            break;
        }
    }

    emit completeChanged();
}

void encodingpage::decodeError(const QString &errstr)
{
    lastError = errstr;
}
