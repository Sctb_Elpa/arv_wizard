#ifndef READPAGE_H
#define READPAGE_H

#include <QWizardPage>

class QLabel;
class QTextEdit;
class QProcess;
class QProgressBar;
class QLabel;
class QRegExp;

class ReadPage : public QWizardPage
{
    Q_OBJECT
public:
    explicit ReadPage(QWidget *parent = 0);

    void initializePage(); // обновить страницу, когда нажимали next
    bool isComplete() const; // вернуть, готова-ли страница
    // нужно посылать сигнал completeChanged(), тогда


private slots:
    void startReading();
    void resetWizardButtons();
    void ParceOutput();
    void readProcessFinised();

    void readError(const QString& errstr);

private:
    void update_progress(const QRegExp &progress_rx);

    QLabel *status;
    QTextEdit *console_window;
    QProcess *Shell;
    QProgressBar* progress;

    QString lastError;

    float done_pr;
    int done_segments;
};

#endif // READPAGE_H
