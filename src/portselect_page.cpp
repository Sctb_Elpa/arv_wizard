#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QTextEdit>
#include <QProcess>
#include <QComboBox>
#include <QPushButton>
#include <QStringList>
#include <QDir>
#include <QRegExp>
#include <QMessageBox>
#include <QTextCodec>

#include <SerialDeviceEnumerator.h>

#include "portselect_page.h"

#define FT232_VID   0x0403 // Идентификатор производителя FTDI
#define FT232_PID   0x6001 // Идентификатор микросхемы FT232RL

static QStringList getPortsAvalable(int* firstFtdi = NULL) {
    SCTBModbusDevice::SerialDeviceEnumerator *enumerator =
            SCTBModbusDevice::SerialDeviceEnumerator::instance();

    SCTBModbusDevice::StringList List = enumerator->devicesAvailable();

    SCTBModbusDevice::StringList::const_iterator it = List.begin();
    int i = 0;

    QTextCodec *textCodec = QTextCodec::codecForLocale();

    QStringList res;
    if (firstFtdi) {
        *firstFtdi = -1;
    }

    while (it != List.end()) {
        enumerator->setDeviceName(*it);
        if (!enumerator->isBusy()) {
            QString p(textCodec->toUnicode((*it).c_str()).trimmed());
#ifdef Q_OS_WIN32
            res.append(QObject::trUtf8("\\\\.\\%1").arg(p));
#else
            res.append(p);
#endif
            if (firstFtdi && *firstFtdi == -1 &&
                    enumerator->vendorID() == FT232_VID &&
                    enumerator->productID() == FT232_PID)
                *firstFtdi = i;
            ++i;
        }

        ++it;
    }

    return res;
}

PortSelect_page::PortSelect_page(QWidget *parent) :
    QWizardPage(parent)
{
    setTitle(trUtf8("Выбор порта"));
    setSubTitle(trUtf8("Необходимо выбрать порт, к которому подключен ваш "
                       "АРВ. Выберите один из портов и нажмите кнопку \"Тест\" "
                       "для поиска устройства на этом порту"));

    QVBoxLayout *mainlayout = new QVBoxLayout;

    QHBoxLayout *controls = new QHBoxLayout;
    controls->addWidget(new QLabel(trUtf8("Порт:")));

    ports = new QComboBox;
    controls->addWidget(ports);
    registerField("port", ports, "currentText");

    testbutton = new QPushButton;
    resetTesstButton();
    controls->addWidget(testbutton);
    controls->addStretch();

    connect(testbutton, SIGNAL(clicked()), this, SLOT(testPort()));

    mainlayout->addLayout(controls);
    statusLabel = new QLabel;
    mainlayout->addWidget(statusLabel);
    mainlayout->addSpacing(10);

    console_window = new QTextEdit;
    console_window->setReadOnly(true);
    mainlayout->addWidget(console_window);

    Shell = NULL;

    portpropertyHolder = new PortpropertyHolder;
    registerField("devAddr", portpropertyHolder, "addr");
    registerField("devSerial", portpropertyHolder, "serial");

    setLayout(mainlayout);
}

void PortSelect_page::initializePage()
{
    console_window->clear();

    int ftdi_index;

    QStringList portsNames = getPortsAvalable(&ftdi_index);

    ports->clear();
    ports->addItems(portsNames);

    if (ftdi_index >= 0) {
        ports->setCurrentIndex(ftdi_index);
    }

    resetFound();
}

bool PortSelect_page::isComplete() const
{
    return portpropertyHolder->getAddr() != 0;
}

void PortSelect_page::found(uint8_t addr, uint16_t serial)
{
    resetWizardCancel();
    statusLabel->setText(trUtf8("Найдено устройство Сер.№ %1, адрес %2").arg(
                             serial).arg(addr));

    portpropertyHolder->setAddr(addr);
    portpropertyHolder->setSerial(serial);
    Shell->kill();

    resetTesstButton();

    emit completeChanged();
}

void PortSelect_page::resetFound()
{
    portpropertyHolder->setAddr(0);
    emit completeChanged();
}

void PortSelect_page::resetTesstButton()
{
    testbutton->setText(trUtf8("Тест"));
    disconnect(testbutton, SIGNAL(clicked()), this,
            SLOT(cancel_search()));
}

void PortSelect_page::showOutput() {
    QByteArray bytes = Shell->readAllStandardOutput();
    QStringList lines = QString(bytes).split("\n");

    QRegExp rx("detected at address \\ ?(\\d+), serial number: \\ ?(\\d+)");

    foreach (QString line, lines) {
        line.replace("\r", QString());
        if (line.isEmpty())
            continue;
        if (rx.indexIn(line) >= 0) {
            found(rx.cap(1).toUInt(), rx.cap(2).toUInt());
        }
        console_window->append(line);
    }
}

void PortSelect_page::resetWizardCancel()
{
    disconnect(wizard()->button(QWizard::CancelButton),
               SIGNAL(clicked()), Shell, SLOT(kill()));
    resetTesstButton();

    if (!isComplete()) {
        statusLabel->setText(trUtf8("Устройств не найдено"));
    }
}

void PortSelect_page::cancel_search()
{
    if (Shell) {
        Shell->kill();
        Shell->deleteLater();
        Shell = NULL;
    }

    resetWizardCancel();

    console_window->append(trUtf8("--- Прервано ---"));
}

void PortSelect_page::testPort()
{
    if (Shell) {
        Shell->kill();
        Shell->deleteLater();
    }

    resetFound();

    console_window->clear();

    Shell = new QProcess(this);
    Shell->setReadChannelMode(QProcess::MergedChannels);
    connect (Shell, SIGNAL(readyReadStandardOutput()),
             this, SLOT(showOutput()));

    QStringList params;
    params << "-p"
           << field("port").toString()
           << "-t";

    Shell->start(QDir::currentPath() + "/ARPFlashDumper",
                 params, QIODevice::ReadOnly);
    if (!Shell->waitForStarted(10000)) {
        QMessageBox::critical(this, trUtf8("Ошибка запуска"),
                              trUtf8("Не удалось запустить дочерний процесс ARPFlashDumper,"
                                     " убедитесь в пристутствии этого файла в каталоге с программой"));
        resetWizardCancel();
    } else {
        connect(wizard()->button(QWizard::CancelButton),
                SIGNAL(clicked()), Shell, SLOT(kill()));
        connect(Shell, SIGNAL(finished(int)), this, SLOT(resetWizardCancel()));

        connect(wizard()->button(QWizard::BackButton), SIGNAL(clicked()), this,
                SLOT(cancel_search()));

        testbutton->setText(trUtf8("Отменить"));
        connect(testbutton, SIGNAL(clicked()), this,
                SLOT(cancel_search()));
    }
}


PortpropertyHolder::PortpropertyHolder(uint8_t addr, uint16_t serial,
                                       QWidget *parent)
    : QWidget(parent)
{
    this->addr = addr;
    this->serial = serial;
}
