#ifndef DUMPSAVESETUP_H
#define DUMPSAVESETUP_H

#include <QWizardPage>

class QLineEdit;

class DumpSaveSetup : public QWizardPage
{
    Q_OBJECT
public:
    explicit DumpSaveSetup(QWidget *parent = 0);

    void initializePage(); // обновить страницу, когда нажимали next
    bool isComplete() const;

private:
    QLineEdit* dumpfilename;

private slots:
    void browseSaveAs();
};

#endif // DUMPSAVESETUP_H
