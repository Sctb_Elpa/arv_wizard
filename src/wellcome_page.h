#ifndef WELLCOME_PAGE_H
#define WELLCOME_PAGE_H

#include <QWizardPage>

class QLabel;

class Wellcome_page : public QWizardPage
{
    Q_OBJECT
public:
    explicit Wellcome_page(QWidget *parent = 0);

private:
    QLabel *label;

};

#endif // WELLCOME_PAGE_H
