#include "wizard.h"
#include <QApplication>
#include <QIcon>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setWindowIcon(QIcon(":/appicon.ico"));
    wizard w;
    w.show();

    return a.exec();
}
