#ifndef WIZARD_H
#define WIZARD_H

#include <QWizard>

class wizard : public QWizard
{
    Q_OBJECT

public:
    wizard(QWidget *parent = 0);
    ~wizard();

    void accept();
};

#endif // WIZARD_H
