#ifndef FINISH_PAGE_H
#define FINISH_PAGE_H

#include <QWizardPage>

class QLabel;
class QCheckBox;
class QPushButton;

class Finish_page : public QWizardPage
{
    Q_OBJECT
public:
    explicit Finish_page(QWidget *parent = 0);

    void initializePage(); // обновить страницу, когда нажимали next

private slots:
    void deleteDumpFile();

private:
    QLabel* devInfo;
    QLabel* dumpfile;
    QPushButton* delDump;
    QLabel* resDir;
};

#endif // FINISH_PAGE_H
