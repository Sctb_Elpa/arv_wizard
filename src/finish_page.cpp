#include <QLabel>
#include <QVariant>
#include <QVBoxLayout>
#include <QPushButton>
#include <QFile>
#include <QRegExp>

#include "finish_page.h"

Finish_page::Finish_page(QWidget *parent) :
    QWizardPage(parent)
{
    setTitle(trUtf8("Готово!"));
    setSubTitle(trUtf8("Считывание и декодирование завершено"));

    QVBoxLayout *layout = new QVBoxLayout;

    QLabel* Summary = new QLabel(trUtf8("Сводка:"));
    layout->addWidget(Summary);
    layout->addSpacing(10);

    devInfo = new QLabel;
    devInfo->setWordWrap(true);
    devInfo->setTextInteractionFlags(Qt::TextSelectableByMouse);
    layout->addWidget(devInfo);
    layout->addSpacing(10);

    dumpfile = new QLabel;
    dumpfile->setWordWrap(true);
    dumpfile->setTextInteractionFlags(Qt::TextSelectableByMouse);
    layout->addWidget(dumpfile);

    delDump = new QPushButton(trUtf8("Удалить дамп-файл (Не рекомендуется)"));
    connect(delDump, SIGNAL(clicked()), this, SLOT(deleteDumpFile()));
    layout->addWidget(delDump);
    layout->addSpacing(10);

    resDir = new QLabel;
    resDir->setWordWrap(true);
    resDir->setTextInteractionFlags(Qt::TextSelectableByMouse);
    layout->addWidget(resDir);

    setLayout(layout);
}

void Finish_page::initializePage()
{
    devInfo->setText(trUtf8("Был прочитан АРВ серийный номер №%1 "
                            ", подключенный к порту %2, "
                            "адрес %3")
                     .arg(field("devSerial").toString())
                     .arg(field("port").toString())
                     .arg(field("devAddr").toString()));
    dumpfile->setText(trUtf8("Дамп файл:\n%1")
                      .arg(field("dumpFileName").toString()));
    resDir->setText(trUtf8("Декодированные результаты в каталоге:\n%1")
                    .arg(field("resultDir").toString()));

    delDump->setEnabled(true);
}

void Finish_page::deleteDumpFile()
{
    QString Dumpfile = field("dumpFileName").toString();
    QFile dumpfile(Dumpfile);
    QFile coeffsfile(Dumpfile.replace(
                       QRegExp("\\.dump$"), ".coeffs"));
    if (dumpfile.exists()) {
        dumpfile.remove();
    }
    if (coeffsfile.exists()) {
        coeffsfile.remove();
    }

    delDump->setEnabled(false);
}
