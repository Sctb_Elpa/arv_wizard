#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QDir>
#include <QFileDialog>
#include <QCheckBox>

#include "encodingsavesetup.h"

encodingSaveSetup::encodingSaveSetup(QWidget *parent) :
    QWizardPage(parent)
{
    setTitle(trUtf8("Сохранение результата"));
    setSubTitle(trUtf8("Необходимо провезти декодирование считанных данных "
                       "Выберите каталог, куда будет сохранены декодированные "
                       "данные"));

    QVBoxLayout *rootLayout = new QVBoxLayout;
    QHBoxLayout *l1 = new QHBoxLayout;

    resultsCatalog = new QLineEdit;
    l1->addWidget(resultsCatalog);
    registerField("resultDir", resultsCatalog, "text");
    connect(resultsCatalog, SIGNAL(textChanged(QString)), this,
            SIGNAL(completeChanged()));

    QPushButton *browse = new QPushButton(trUtf8("..."));
    connect(browse, SIGNAL(clicked()), this, SLOT(browseSaveAs()));

    l1->addWidget(browse);

    rootLayout->addLayout(l1);

    saveHeads = new QCheckBox(trUtf8("Сохранить заголовки сегментов"));
    saveHeads->setToolTip(trUtf8("Записать в текстовые файлы данные из заголовочных записей сегментов"));
    registerField("saveHeads", saveHeads, "tristate");
    rootLayout->addWidget(saveHeads);

    setLayout(rootLayout);
}

void encodingSaveSetup::initializePage()
{
    if (resultsCatalog->text().isEmpty()) {
        resultsCatalog->setText(QDir::homePath() + trUtf8("/ARV_%1-data").arg(
                                    field("devSerial").toString()));
    }
}

bool encodingSaveSetup::isComplete() const
{
    return !resultsCatalog->text().isEmpty() &&
            !resultsCatalog->text().endsWith("/") &&
            QDir(resultsCatalog->text()).cdUp();
}

void encodingSaveSetup::browseSaveAs()
{
    QDir base(resultsCatalog->text());
    if (base.path().isEmpty())
        base = QDir::home();

    QString dialogres =
            QFileDialog::getExistingDirectory(this,
                                              trUtf8("Выбрать каталог..."),
                                              base.path());

    if (!dialogres.isEmpty()) {
        resultsCatalog->setText(dialogres);
    }
}
