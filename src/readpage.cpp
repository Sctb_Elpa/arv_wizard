#include <QVBoxLayout>
#include <QTextEdit>
#include <QProcess>
#include <QProgressBar>
#include <QDir>
#include <QTimer>
#include <QAbstractButton>
#include <QDebug>
#include <QMessageBox>
#include <QFileDialog>
#include <QTime>
#include <QLabel>

#include "readpage.h"

#define SEC_IN_DAY (60*60*24)

static const QChar zero_char('0');

ReadPage::ReadPage(QWidget *parent) :
    QWizardPage(parent)
{
    setTitle(trUtf8("Чтение данных"));
    setSubTitle(trUtf8("Идет чтение данных с АРВ"));

    QVBoxLayout *rootLayout = new QVBoxLayout;

    status = new QLabel;
    rootLayout->addWidget(status);

    progress = new QProgressBar;
    progress->setMaximum(0);
    progress->setMaximum(100);

    rootLayout->addWidget(progress);
    rootLayout->addSpacing(10);

    console_window = new QTextEdit;
    console_window->setReadOnly(true);
    rootLayout->addWidget(console_window);

    Shell = NULL;

    setLayout(rootLayout);
}

void ReadPage::initializePage()
{
    progress->setValue(0);

    QTimer::singleShot(10, this, SLOT(startReading()));

    done_pr = 0;
    done_segments = 0;
}

bool ReadPage::isComplete() const
{
    return lastError.isEmpty() && progress->value() == 100;
}

void ReadPage::startReading()
{
    progress->setValue(0);

    if (Shell) {
        Shell->kill();
        Shell->deleteLater();
    }

    console_window->clear();

    Shell = new QProcess(this);
    Shell->setReadChannelMode(QProcess::MergedChannels);
    connect (Shell, SIGNAL(readyReadStandardOutput()),
             this, SLOT(ParceOutput()));

    QStringList params;
    QString exe(QDir::currentPath() + "/ARPFlashDumper");
    params
            << "-p"
            << field("port").toString()
            << "-d"
            << field("devAddr").toString()
            << "-o"
            << field("dumpFileName").toString()
           #ifdef  QT_DEBUG
            << "--to=0"
           #endif
               ;

    Shell->start(exe, params, QIODevice::ReadOnly);

    if (!Shell->waitForStarted(10000)) {
        QMessageBox::critical(this, trUtf8("Ошибка запуска"),
                              trUtf8("Не удалось запустить дочерний процесс ARPFlashDumper,"
                                     " убедитесь в пристутствии этого файла в каталоге с программой"));
        resetWizardButtons();
    } else {
        connect(wizard()->button(QWizard::CancelButton), SIGNAL(clicked()), Shell,
                SLOT(kill()));
        connect(wizard()->button(QWizard::BackButton), SIGNAL(clicked()), Shell,
                SLOT(kill()));
        connect(Shell, SIGNAL(finished(int)), this, SLOT(readProcessFinised()));
    }
}

void ReadPage::resetWizardButtons()
{
    disconnect(wizard()->button(QWizard::CancelButton),
               SIGNAL(clicked()), Shell, SLOT(kill()));
    disconnect(wizard()->button(QWizard::BackButton), SIGNAL(clicked()), Shell,
               SLOT(kill()));
}

void ReadPage::update_progress(const QRegExp &progress_rx)
{
    float done_pr_new = progress_rx.cap(3).toFloat();
    int done_segments_new = progress_rx.cap(1).toUInt();
    if ((done_segments == done_segments_new) &&
            (done_pr_new - done_pr < 0.01))
        return;

    done_pr = done_pr_new;
    done_segments = done_segments_new;

    QTime workTime(
                progress_rx.cap(5).toUInt(),
                progress_rx.cap(6).toUInt(),
                progress_rx.cap(7).toUInt()
                );

    int progress_val_pr = qRound(done_pr);
    if (progress->value() != progress_val_pr) {
        if (progress_val_pr == 100) {
            status->setText(trUtf8("Выполнено"));
            return;
        } else {
            progress->setValue(progress_val_pr);
        }
    }

    uint work_days = progress_rx.cap(4).toUInt();

    uint workTime_sec = QTime().secsTo(workTime) +
            work_days * SEC_IN_DAY;

    if (!workTime_sec || done_pr == 0.0) {
        status->setText(trUtf8("Осталось примерно вычисляется"));
        return;
    }

    uint forecast = qRound(workTime_sec * 100.0 / done_pr) - workTime_sec;
    uint days = forecast / SEC_IN_DAY;
    QTime forecast_t = QTime().addSecs(forecast - days * SEC_IN_DAY);

    status->setText(trUtf8("Осталось примерно %1д. %2:%3:%4")
                    .arg(days - work_days)
                    .arg(forecast_t.hour(), 2, 10, zero_char)
                    .arg(forecast_t.minute(), 2, 10, zero_char)
                    .arg(forecast_t.second(), 2, 10, zero_char));
}

void ReadPage::ParceOutput()
{
    QByteArray bytes = Shell->readAllStandardOutput();
    QStringList lines = QString(bytes).split("\n");

    QRegExp fatalError("(\\[FATAL\\]|\\[ERROR\\])\t(.*)");
    QRegExp progress_rx("Reading block:.*(\\d+)\\/(\\d+).*(\\d+[,.]\\d+)%.*(\\d+) (\\d{2}):(\\d{2}):(\\d{2})");

    foreach (QString line, lines) {
        line.replace("\r", QString());
        if (line.isEmpty()) {
            continue;
        }

        if (fatalError.indexIn(line) >= 0) {
            readError(fatalError.cap(2));
            console_window->append(line);
            continue;
        }

        if (progress_rx.indexIn(line) >= 0) {
            update_progress(progress_rx);
            console_window->append(progress_rx.cap(0));
            continue;
        }
        console_window->append(line);
    }
}

void ReadPage::readProcessFinised()
{
    resetWizardButtons();

    progress->setValue(100);

    if (!lastError.isEmpty()) {
        int res = QMessageBox::critical(this, trUtf8("Ошибка чтения"), trUtf8(
                                            "Во время чтения произошла ошибка \"%1\" "
                                            "которая привела к невозможности продолжить."
                                            "\nРекомендуется сохранить журнал для изучения"
                                            "причины ошибки.").arg(lastError),
                                        trUtf8("Закрыть"), trUtf8("Повтор"), trUtf8("Сохранить и закрыть"));
        switch (res) {
        case 1:
            lastError.clear();
            initializePage();
            break;
        case 2:
            QString logfile =
                    QFileDialog::getSaveFileName(this,
                                                 trUtf8("Сохранить отчет"), "",
                                                 "*.log");
            if (!logfile.isEmpty()) {
                if (!logfile.endsWith(".log")) {
                    logfile.append(".log");
                }
                QFile file(logfile);

                if (file.open(QIODevice::ReadWrite)) {
                    QTextStream stream(&file);
                    stream << console_window->toPlainText();
                    file.flush();
                    file.close();
                } else {
                    QMessageBox::critical(this, trUtf8("Ошибка записи"),
                                          trUtf8("Не удалось записать отчет в файл %1")
                                          .arg(logfile));
                    return;
                }
            }
            break;
        }
    }

    emit completeChanged();
}

void ReadPage::readError(const QString &errstr)
{
    lastError = errstr;
}
