#ifndef ENCODINGSAVESETUP_H
#define ENCODINGSAVESETUP_H

#include <QWizardPage>

class QLineEdit;
class QCheckBox;

class encodingSaveSetup : public QWizardPage
{
    Q_OBJECT
public:
    explicit encodingSaveSetup(QWidget *parent = 0);

    void initializePage(); // обновить страницу, когда нажимали next
    bool isComplete() const;

private:
    QLineEdit* resultsCatalog;
    QCheckBox* saveHeads;

private slots:
    void browseSaveAs();
};

#endif // ENCODINGSAVESETUP_H
