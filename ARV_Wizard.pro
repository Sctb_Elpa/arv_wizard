#-------------------------------------------------
#
# Project created by QtCreator 2016-03-18T09:06:04
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ARV_Wizard
TEMPLATE = app


SOURCES += main.cpp\
        wizard.cpp \
    wellcome_page.cpp \
    portselect_page.cpp \
    dumpsavesetup.cpp \
    readpage.cpp \
    encodingsavesetup.cpp \
    encodingpage.cpp \
    finish_page.cpp

HEADERS  += wizard.h \
    wellcome_page.h \
    portselect_page.h \
    dumpsavesetup.h \
    readpage.h \
    encodingsavesetup.h \
    encodingpage.h \
    finish_page.h

unix:INCLUDEPATH += /usr/local/include/SCTBModbusDevice
unix:LIBS += -L/usr/local/lib -lSCTBModbusDevice

win32:INCLUDEPATH += H:/Razrab/C/sctbmodbusdevice/src
win32:LIBS += H:/Razrab/C/sctbmodbusdevice/creator-build/src/libSCTBModbusDevice_static.a
